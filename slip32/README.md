# slip32

Haskell implementation of the SLIP-0032 standard: Extended serialization format for BIP-32 wallets.

Based on the [draft SLIP-0032 spec](https://github.com/satoshilabs/slips/blob/71a3549388022820e77aa1f44c80d0f412e5529f/slip-0032.md).

