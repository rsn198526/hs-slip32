{ mkDerivation, base, base16-bytestring, bech32, binary, bip32
, bitcoin-keys, bytestring, hedgehog, stdenv, tasty, tasty-hedgehog
, tasty-hunit, text, nix-gitignore
}:
mkDerivation {
  pname = "slip32";
  version = "0.2.1";
  src = nix-gitignore.gitignoreSourcePure ../.gitignore ./.;
  libraryHaskellDepends = [
    base bech32 binary bip32 bitcoin-keys bytestring text
  ];
  testHaskellDepends = [
    base base16-bytestring bip32 bitcoin-keys bytestring hedgehog tasty
    tasty-hedgehog tasty-hunit text
  ];
  homepage = "https://gitlab.com/k0001/hs-slip32";
  description = "SLIP-0032: Extended serialization format for BIP-32 wallets";
  license = stdenv.lib.licenses.asl20;
}
