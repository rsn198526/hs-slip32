# Version 0.2.1

* Now compatible with `bip32-0.2`.

* Depend on the `bitcoin-keys` library.


# Version 0.2

* COMPILER ASSISTED BREAKING CHANGE — Not exported anymore: `Pub`, `pub`,
  `unPub`, `Prv`, `prv`, `unPrv`, `Chain`, `chain`, `unChain`. These types are
  now available in the `bip32` library.

* BREAKING CHANGE POSSIBLY REQUIRING HUMAN INTERVENTION — `prv` and `unPrv` from
  the `bip32` library deal with `32`-byte long `ByteString`s now, rather than
  `33` bytes as before. The leading `0x00` byte has now been dropped.

* BREAKING CHANGE: `Path` now deals with values of type `Index` from the `bip32`
  library, rather than `Word32`.

* Depend on the `bip32` library.

* Builds on GHCJS.


# Version 0.1

* Initial version.
