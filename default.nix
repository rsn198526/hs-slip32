let pkgs = import ./nix;
in rec {
  ghc883 = pkgs._here.ghc883.slip32;
  ghc865 = pkgs._here.ghc865.slip32;
  ghcjs86 = pkgs._here.ghcjs86.slip32;
}
